    
    ### EscPosEncoder 라이브러리
    
    #### index.d.ts 
    ```
    start(): any;
    end(): any;
    korean(): any;
    english(): any;
    ```

    #### esc-pos-encoder.js 
    ```
    start() {
        this._queue([
            0x1e, 0x53,
        ]);

        return this;
    }

    end() {
        this._queue([
            0x1e, 0x45,
        ]);

        return this;
    }

    korean() {
        this._queue([
            0x1e, 0x4C, 0x30
        ]);

        return this;
    }
    
    english() {
        this._queue([
            0x1e, 0x4C, 0x30
        ]);

        return this;
    }
    ```

    - hanzi delete



    - IOS Workspace 
    Version 1.0.13
    Build 1.2.12