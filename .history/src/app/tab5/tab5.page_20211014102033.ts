import { LanguageModel } from './../providers/models/language.model';
import { Component, NgZone, OnInit } from '@angular/core';
import { AuthenticationService } from '../providers/services/authentication.service';
import { GetApiService } from '../providers/services/get-api.service';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment.prod';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PostApiService } from '../providers/services/post-api.service';
import { SupportService } from '../providers/services/support.service';
import { AlertController, ActionSheetController, ModalController, NavController, Platform } from '@ionic/angular';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { TranslateConfigService, StorageService } from '../providers/providers';
import { TranslateService } from '@ngx-translate/core';
import { BLE } from '@ionic-native/ble/ngx';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {

  UUID_SERVICE = environment.uuid_service;
  WRITE_CHARACTERISTIC = environment.write_characteristic;
  READ_CHARACTERISTIC = environment.read_characteristic;

  commonForm: FormGroup;

  profiles : any;
  device : string;
  commons : any;

  USERID = environment.user_id;
  DEVICEID = environment.device_id;
  LOCKER = environment.locker;

  isEditItems: boolean = true;

  showCommon: boolean = false;

  versionNumber:string;
  languageSelected : string = '';
  languages : Array<LanguageModel>;

  realtime: string = '0';
  private interval = null;
  isConnected = false;
  peripheral: any = {};
  scaleUUID: string;
  scaleName: string;

  constructor(
    private authService: AuthenticationService,
    private storage: Storage,
    private storageService: StorageService,
    public getapi: GetApiService,
    private formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    private postapi: PostApiService,
    public support:SupportService,
    public actionSheetController: ActionSheetController,
    public modalCtrl: ModalController,
    private appVersion: AppVersion,
    public navCtrl: NavController,
    public translateConfig: TranslateConfigService,
    private translate: TranslateService,
    private platform: Platform,
    private ngZone: NgZone,
    private ble: BLE,
  ) {
    this.languages = this.translateConfig.getLanguages();

    this.getProfile();
    this.getCommon();
    this.setLanguage();

    this.appVersion.getVersionNumber().then(value => {
      this.versionNumber = `v${value}`;
    }).catch(err => {
      console.log(err);
    });
  }

  ionViewDidEnter(){
    this.storageService.getLang().then(lang => {
      if (!lang) {
        const localLang = this.translate.getBrowserLang();
        if(localLang != 'ko') {
          this.languageSelected = 'en';
        } else {
        this.languageSelected = this.translate.getBrowserLang();
        }
      } else {
        this.languageSelected = lang;
      }
    });

    this.storage.get(this.DEVICEID).then(device => {
      this.getapi.bluetoothUUID(device).subscribe((res) => {
        if (this.platform.is('ios')) {  
          this.scaleName = res[0].scale_name;
        } else if(this.platform.is('android')){
          this.scaleUUID = res[0].scale_uuid;
        }
      });
    });

    this.storage.ready().then(() => {
      this.storage.get(this.DEVICEID).then(device => {
        this.device = device;
        this.getapi.getCommon(device).subscribe((res) => {
          this.commonForm = this.formBuilder.group({
            mass: new FormControl(res[0].mass, Validators.required),
            water: new FormControl(res[0].water, Validators.required),
          });
        });
      });
    });
  }

  ionViewWillLeave() {
    this.disconnected();
  }

  ionViewDidLeave(){
    this.disconnected();
  }
 
  ionViewWillUnload(){
    this.disconnected();
  }

  ngOnDestroy() {
    this.disconnected();
  }

  ngOnInit() {
    this.scan();
  }
  
  scan() {
    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device), 
      error => this.support.presentToast('블루투스 스캔 오류')
    );
  }

  onDeviceDiscovered(device) {
    console.log('Discovered ' + JSON.stringify(device, null, 2));
    if(device.name == this.scaleName) {
      this.scaleUUID = device.id;
    }
  }

  getProfile() {
    this.storage.get(this.USERID).then(username => {
      this.getapi.getProfile(username).subscribe((res) => {
        this.profiles = res;
      });
    });
  }

  getCommon() {
    this.storage.get(this.DEVICEID).then(device => {
      this.getapi.getCommon(device).subscribe((res) => {
        this.commons = res;
      });
    });
  }

  onEditCloseItems() {
    this.isEditItems = !this.isEditItems;
    if(!this.isEditItems) {
      this.checkConnection(this.scaleUUID);
    } else {
      this.disconnected();
    }
  }

  onSubmit(data) {
    this.storage.get(this.DEVICEID).then(deviceId => {
      this.postapi.updateCommon(deviceId, data)
      .subscribe(
        res => {
          this.support.presentToast(this.translate.instant("TAB5.CHANGED"));
          this.getCommon();
          this.isEditItems = true;
        }
      );
    }); 
  }

  async changePassword() {
    const alert = await this.alertCtrl.create({
      header: this.translate.instant("TAB5.PASS.TITLE"),
      inputs: [
        {
          type: 'password',
          name: 'oldpass',
          placeholder: this.translate.instant("TAB5.PASS.OLD")
        },
        {
          type: 'password',
          name: 'newpass',
          placeholder: this.translate.instant("TAB5.PASS.NEW")
        },
        {
          type: 'password',
          name: 'newpasschk',
          placeholder: this.translate.instant("TAB5.PASS.RE")
        }
      ],
      buttons: [
        {
          text: this.translate.instant('CANCEL'),
          role: 'cancel'
        },
        {
          text: this.translate.instant('CONFIRM'),
          role: 'submit',
          handler: data => {
            
            if(data.newpass == "" || data.oldpass == "" || data.newpasschk == "") {
              this.support.presentToast(this.translate.instant('INPUTALL'));
              return false;
            }
            if(data.newpass != data.newpasschk) {
              this.support.presentToast(this.translate.instant('INCORRECT'));
              return false;
            } else {
              let value = { "oldpass": data.oldpass, "newpass": data.newpass };
              
              this.storage.get(this.USERID).then(id => {
                this.authService.updatePass(id, value).subscribe( (res: any) => {  
                  this.support.presentToast(res.message);
                  
                });
              });
            }
            
          }
        }
      ],
    });
    await alert.present();
  }

  onLoggedout() {
    this.authService.logout();
  }

  moveLogin() {
    this.navCtrl.navigateRoot('/login');
  }

  setLanguage() {
    this.translateConfig.setLanguage(this.languageSelected);
    this.storageService.setLang(this.languageSelected);
  }

  apply(evt) {
    const id = evt.target.id;
    if(id == 'mass') {
      this.commonForm.controls['mass'].patchValue(this.realtime);
    } else if(id == 'water') {
      this.commonForm.controls['water'].patchValue(this.realtime);
    }
  }

  async checkConnection(seleccion) {
    await this.disconnected();
    this.ble.connect(seleccion).subscribe(
      peripheral => {
        this.ngZone.run(() => {
          this.interval = setInterval(() => {      
            this.onConnected(peripheral)
          }, 1*1000);
        });
        this.isConnected = true;
        this.support.presentToast('장치와 연결되었습니다.');
      },
      peripheral => {
        try {
          this.onDeviceDisconnected(peripheral);
        } catch (err) {
          throw new Error(err);
        }
      }
    );
  }

  async onDeviceDisconnected(peripheral) {
    this.support.presentToast('장치를 연결할수 없습니다.');
    let alert = this.alertCtrl.create({
      message: '장비와 연결을 실패했습니다. \n전원을 켜시고 재연결 버튼을 누르세요.',
      header: this.translate.instant("ALERT"),
      buttons: [
        {
          text: this.translate.instant("RECONNECT"),
          role: 'submit',
          handler: _ => {
            this.ble.stopNotification(peripheral.id, this.UUID_SERVICE, this.READ_CHARACTERISTIC)
            .catch(err => console.error(err));
          }
        }
      ]
    });
    alert.then(alert => alert.present());
    this.isConnected = false;
  }

  onConnected(peripheral) {
    this.ngZone.run(() => {
      this.peripheral = peripheral;
    });
    var inputdata = new Uint8Array(3);
    inputdata[0] = 0x02;
    inputdata[1] = 0x20;
    inputdata[2] = 0x03;
    
    this.ble
        .writeWithoutResponse(
          this.peripheral.id, 
          this.UUID_SERVICE, 
          this.WRITE_CHARACTERISTIC, 
          inputdata.buffer
        )
        .then(
          data => {
            this.subscribe();
          },
          err => {
            throw new Error(err);
          }
    );
  }

  subscribe() {
    this.ble
        .startNotification(this.peripheral.id, this.UUID_SERVICE, this.READ_CHARACTERISTIC).subscribe(
      data => {
        this.onValueChange(data[0]);
      },
      e => console.error('noti error'+ e)
    );
  }

  onValueChange(buffer: ArrayBuffer) {
    try {
      let data = this.support.bytesToString(buffer);
      this.ngZone.run(() => {
        this.realtime = data.replace(/[^0-9]/g,'');
        console.log(this.reatime);
      });
    } catch (e) {
      throw new Error(e);
    }
  }

  disconnected() {
    clearInterval(this.interval);
    this.ble.stopNotification(this.peripheral.id, this.UUID_SERVICE, this.READ_CHARACTERISTIC)
    .catch(err => console.error(err));
    this.ble.disconnect(this.peripheral.id).then(
      () => console.log('Disconnected ' + JSON.stringify(this.peripheral, null, 2)),
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral, null, 2))
    )
  }

}
