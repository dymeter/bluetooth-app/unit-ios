import { Injectable } from '@angular/core';
import { MenuController, ModalController, NavController, ToastController, AlertController, LoadingController, Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class SupportService {

  constructor(
      public navCtrl:NavController,
      private toastController: ToastController,
      public alertCtrl: AlertController,
      public loadingController: LoadingController,
      private platform: Platform,
    ) { }
  

  async presentToast(msg) {
    const toast = await this.toastController.create({
        message: msg,
        position: 'bottom',
        duration: 1200
    });
    toast.present();
  }

  async presentToastTop(msg) {
    const toast = await this.toastController.create({
        message: msg,
        position: 'top',
        duration: 1200
    });
    toast.present();
  }

  showAlert(msg) {
    let alert = this.alertCtrl.create({
      message: msg,
      header: '알림',
      buttons: ['확인']
    });
    alert.then(alert => alert.present());
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      duration: 2000,
      spinner: 'dots',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async presentBleLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'custom-class',
      message: '블루투스 연결중...',
      duration: 3000
    });
    return await loading.present();
  }

  isIos(): boolean {
    return this.platform.is('ios');
  }

  isAndroid(): boolean {
    return this.platform.is('android');
  }

  bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }

}
