import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import { FormulaService } from 'src/app/providers/services/formula.service';
import { environment } from 'src/environments/environment.prod';
import { GetApiService } from 'src/app/providers/services/get-api.service';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { SupportService } from 'src/app/providers/services/support.service';
import { BLE } from '@ionic-native/ble/ngx';

@Component({
  selector: 'app-air',
  templateUrl: './air.component.html',
  styleUrls: ['./air.component.scss']
})
export class AirComponent implements OnInit {
  @ViewChild('modelInput2', {static: false}) modelInput2;
  @ViewChild('modelInput3', {static: false}) modelInput3;

  UUID_SERVICE = environment.uuid_service;
  WRITE_CHARACTERISTIC = environment.write_characteristic;
  READ_CHARACTERISTIC = environment.read_characteristic;

  DEVICEID = environment.device_id;
  scaleUUID: string;
  airmeterUUID: string;
  scaleName: string;
  airmeterName: string;

  form: FormGroup;
  value: string = '';
  footnote: string;
  realtime: string = '0';
  pressure: number[] = [0, 0];
  inputs: string[] = ['0', '0'];
  
  isConnected = false;
  private interval = null;
  peripheral: any = {};

  constructor(
    public modalCtrl: ModalController,
    public formula: FormulaService,
    private formBuilder: FormBuilder,
    private storage: Storage,
    public getapi: GetApiService,
    private translate: TranslateService,
    private ble: BLE,
    public support: SupportService,
    private ngZone: NgZone,
    public alertCtrl: AlertController,
  ) { }

  ionViewWillEnter(){
    this.storage.get(this.DEVICEID).then(device => {
      this.getapi.bluetoothUUID(device).subscribe((res) => {
        this.scaleName = res[0].scale_name;
        this.airmeterName = res[0].airmeter_name;
      });
    });
  }

  ngOnInit() {
    this.scan();
    this.form = this.formBuilder.group({
      concrete: ['', [Validators.required]],
      water: ['', [Validators.required]],
      iPressure: ['', [Validators.required]],
      ePressure: ['', [Validators.required]],
      coefficient: ['0', [Validators.required]],
    });
  }

  ionViewWillLeave(){
    this.disconnected();
  }

  dismiss(data?: any) {
    this.disconnected();
    this.modalCtrl.dismiss(data);
  }

  disconnected() {
    clearInterval(this.interval);
    this.ble.stopNotification(this.peripheral.id, this.UUID_SERVICE, this.READ_CHARACTERISTIC);
    this.ble.disconnect(this.peripheral.id).then(
      () => console.log('Disconnected ' + JSON.stringify(this.peripheral, null, 2)),
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral, null, 2))
    )
  }

  scan() {
    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device), 
      error => this.support.presentToast('블루투스 스캔 오류')
    );
  }

  onDeviceDiscovered(device) {
    console.log('Discovered ' + JSON.stringify(device, null, 2));
    if(device.name == this.scaleName) {
      this.scaleUUID = device.id;
    }
    if(device.name == this.airmeterName) {
      this.airmeterUUID = device.id;
    }
  }

  calculator() {
    let concrete = this.form.controls['concrete'].value;
    let water = this.form.controls['water'].value;
    let iPressure = this.form.controls['iPressure'].value;
    let ePressure = this.form.controls['ePressure'].value;
    let coefficient = this.form.controls['coefficient'].value;

    this.value = this.formula.air(concrete, water, iPressure, ePressure, coefficient);
  }

  onEvent(ev) {
    let name = ev.target.name;
    switch(name) {
      case "concrete":
        this.support.presentBleLoading();
        this.footnote = this.translate.instant('TAB4.AIR.APPLY_BUTTON_1');
        this.checkConnection(this.scaleUUID);
        break;
      case "water":
        this.footnote = this.translate.instant('TAB4.AIR.APPLY_BUTTON_2');
        this.checkConnection(this.scaleUUID);
        break;
      case "iPressure":
        this.footnote = this.translate.instant('TAB4.AIR_BUTTON');
        this.checkConnection(this.airmeterUUID);
        break;
      case "ePressure":
        this.footnote = this.translate.instant('TAB4.AIR_BUTTON');
        this.checkConnection(this.airmeterUUID);
        break;
      default: 
        this.footnote = this.translate.instant('TAB4.NO_MESSAGE');
    }
  }

  async checkConnection(seleccion) {
    await this.disconnected();
    this.ble.connect(seleccion).subscribe(
      peripheral => {
        this.ngZone.run(() => {
          this.interval = setInterval(() => {      
            this.onConnected(peripheral)
          }, 1*1000);
        });
        this.isConnected = true;
        this.support.presentToast('장치와 연결되었습니다.');
      },
      peripheral => this.onDeviceDisconnected(peripheral)
    );
  }

  onConnected(peripheral) {
    this.ngZone.run(() => {
      this.peripheral = peripheral;
    });
    var inputdata = new Uint8Array(3);
    inputdata[0] = 0x02;
    inputdata[1] = 0x20;
    inputdata[2] = 0x03;
    
    this.ble
        .writeWithoutResponse(
          this.peripheral.id, 
          this.UUID_SERVICE, 
          this.WRITE_CHARACTERISTIC, 
          inputdata.buffer
        )
        .then(
          data => {
            this.subscribe();
          },
          err => {
            console.error(err);
          }
    );
  }

  subscribe() {
    this.ble
        .startNotification(this.peripheral.id, this.UUID_SERVICE, this.READ_CHARACTERISTIC).subscribe(
      data => {
        this.onValueChange(data[0]);
      },
      e => console.error('noti error'+ e)
    );
  }

  onValueChange(buffer: ArrayBuffer) {
    this.ngZone.run(() => {
      try {
        let data = this.support.bytesToString(buffer);
        if(data.length == 14) {
          this.ngZone.run(() => {
            this.realtime = data.replace(/[^0-9]/g,'');          
          });
        } else if(data.length == 22) {
          this.ngZone.run(() => {
            this.pressure[0] = data.substring(6, 11);
            this.pressure[1] = data.substring(16, 21);
          });
        }
      } catch (e) {
        throw new Error(e);
      }
    });
  }

  async onDeviceDisconnected(peripheral) {
    this.support.presentToast('장치를 연결할수 없습니다.');
    let alert = this.alertCtrl.create({
      message: '장비와 연결을 실패했습니다. \n전원을 켜시고 재연결 버튼을 누르세요.',
      header: this.translate.instant("ALERT"),
      buttons: [
        {
          text: this.translate.instant("RECONNECT"),
          role: 'submit',
          handler: _ => {
            this.ble.stopNotification(peripheral.id, this.UUID_SERVICE, this.READ_CHARACTERISTIC)
            .catch(err => console.error(err));
          }
        }
      ]
    });
    alert.then(alert => alert.present());
    this.isConnected = false;
  }

  apply() {
    this.inputs[0] = this.realtime;
    this.modelInput2.setFocus();
  }

  apply2() {
    this.inputs[1] = this.realtime;
    this.modelInput3.setFocus();
  }

}
