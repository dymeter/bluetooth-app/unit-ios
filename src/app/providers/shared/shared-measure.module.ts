import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { EditMixComponent, MeasureComponent, ResultComponent } from 'src/app/tab2/components';
import { ScanComponent } from 'src/app/components/scan/scan.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TranslateModule.forChild(),
        ReactiveFormsModule,
    ],
    exports: [
        EditMixComponent,
        MeasureComponent,
        ResultComponent,
        ScanComponent
    ],
    declarations: [
        EditMixComponent,
        MeasureComponent,
        ResultComponent,
        ScanComponent
    ]
})
export class SharedMeasureModule { }
