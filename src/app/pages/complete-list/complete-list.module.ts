import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CompleteListPage } from './complete-list.page';
import { TranslateModule } from '@ngx-translate/core';
import { SharedMeasureModule } from 'src/app/providers/shared/shared-measure.module';
import { ScanComponent } from 'src/app/components/scan/scan.component';

const routes: Routes = [
  {
    path: '',
    component: CompleteListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedMeasureModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [CompleteListPage],
})
export class CompleteListPageModule {}
