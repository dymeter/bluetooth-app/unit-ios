import { SupportService } from './../../providers/services/support.service';
import { Component, NgZone } from '@angular/core';
import { Storage } from '@ionic/storage';
import { GetApiService } from 'src/app/providers/services/get-api.service';
import { environment } from 'src/environments/environment.prod';
import { TranslateService } from '@ngx-translate/core';
import { AlertController, Platform } from '@ionic/angular';
import EscPosEncoder from 'esc-pos-encoder-ionic';
import { BLE } from '@ionic-native/ble/ngx';

@Component({
  selector: 'app-bluetooths',
  templateUrl: './bluetooths.page.html',
  styleUrls: ['./bluetooths.page.scss'],
})
export class BluetoothsPage {

  DEVICEID = environment.device_id;
  UUID_SERVICE = environment.uuid_service;
  WRITE_CHARACTERISTIC = environment.write_characteristic;
  READ_CHARACTERISTIC = environment.read_characteristic;

  uuids: any;
  isConnected = false;
  realtime: string = '0';
  pressure: string[] = ['0', '0'];
  private interval = null;
  peripheral: any = {};

  message = '프린터 테스트입니다.';
  plt: boolean;

  constructor(
    public getapi: GetApiService,
    private storage: Storage,
    private translate: TranslateService,
    private ble: BLE,
    private alertCtrl: AlertController,
    private support: SupportService,
    private ngZone: NgZone,
    private platform: Platform,
  ) { 
    
  }

  ionViewDidEnter(){
    this.scan();
  }
  
  ionViewWillEnter(){
    this.storage.get(this.DEVICEID).then(device => {
      this.getapi.bluetoothUUID(device).subscribe((res) => {
        this.uuids = res;
      });
    });
    this.plt = this.platform.is('android');
  }

  scan() {
    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device), 
      error => this.scanError(error)
    );
  }

  onDeviceDiscovered(device) {
    console.log('Discovered ' + JSON.stringify(device, null, 2));
  }

  async scanError(error) {
    this.support.presentToast('블루투스 스캔 오류');
  }
  
  deviceSelected(id) {
    this.ble.connect(id).subscribe(
      peripheral => {
        this.interval = setInterval(() => {      
          this.onConnected(peripheral)
        }, 1*1000);
        this.support.presentToast('장치와 연결되었습니다.');
        this.isConnected = true;
      },
      peripheral => this.onDeviceDisconnected(peripheral)
    );
  }

  onConnected(peripheral) {
    this.ngZone.run(() => {
      this.peripheral = peripheral;
    });
    var inputdata = new Uint8Array(3);
    inputdata[0] = 0x02;
    inputdata[1] = 0x20;
    inputdata[2] = 0x03;
    
    this.ble
        .writeWithoutResponse(
          this.peripheral.id, 
          this.UUID_SERVICE, 
          this.WRITE_CHARACTERISTIC, 
          inputdata.buffer
        )
        .then(
          data => {
            this.subscribe();
          },
          err => {
            console.error(err);
          }
    );
  }

  subscribe() {
    this.ble
        .startNotification(this.peripheral.id, this.UUID_SERVICE, this.READ_CHARACTERISTIC).subscribe(
      data => {
        this.onValueChange(data[0]);
      },
      e => console.error('noti error'+ e)
    );
  }

  onValueChange(buffer: ArrayBuffer) {
    this.ngZone.run(() => {
      try {
        let data = this.support.bytesToString(buffer);
        if(data.length == 22) {
          this.pressure[0] = data.substring(6, 11);
          this.pressure[1] = data.substring(16, 21);
        } else {
          this.realtime = data.replace(/[^0-9]/g,'');          
        }
      } catch (e) {
        console.error(e);
      }
    });
  }

  async onDeviceDisconnected(peripheral) {
    this.support.presentToast('장치를 연결할수 없습니다.');
    this.scan();
  }

  sendMessage(message) {
    const encoder = new EscPosEncoder();
    const result = encoder.initialize();
    result
    .codepage('cp949')
    .line(message)
    .newline()
    .newline()
    .newline()

    this.printData(result.encode())
    .then((printStatus) => {
      console.log(printStatus);
    });
  }

  printData(data: Uint8Array) {
    return this.ble.writeWithoutResponse(
      this.peripheral.id, 
      this.UUID_SERVICE, 
      this.WRITE_CHARACTERISTIC, 
      data.buffer
    );
  }

  ionViewWillLeave(){
    clearInterval(this.interval);
    this.ble.disconnect(this.peripheral.id).then(
      () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
    )
  }

  disconnect() {
    this.ble.disconnect(this.peripheral.id).then(
      () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
    )
  }

  async alertBox(device) {
    const alert = await this.alertCtrl.create({
      header: this.translate.instant('BLUETOOTH.ALERTS.CONNECT.TITLE'),
      message: this.translate.instant('BLUETOOTH.ALERTS.CONNECT.MESSAGE'),
      buttons: [
        {
          text: this.translate.instant('CANCEL'),
          role: 'cancel',
        },
        {
          text: this.translate.instant('ACCEPT'),
          handler: () => {
            this.deviceSelected(device);
          }
        }
      ]
    });
    await alert.present();
  }

}
